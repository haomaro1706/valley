<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\API\APIBaseController as APIBaseController;
use App\Sopa;
use Validator;

class SopaAPIController extends APIBaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sopas = Sopa::all();
        return $this->sendResponse($sopas->toArray(), 'sopas listados con exito.');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $validator = Validator::make($input, [
            'nombre' => 'required',
            'estado' => 'required'
        ]);
        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }
        $sopas = Sopa::create($input);
        return $this->sendResponse($sopas->toArray(), 'Sopa creado correctamente.');
    }
    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $sopas = Sopa::find($id);
        if (is_null($sopas)) {
            return $this->sendError('Sopa no encontrado.');
        }
        return $this->sendResponse($sopas->toArray(), 'sopas generados correctamente.');
    }
    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->all();
        $validator = Validator::make($input, [
            'nombre' => 'required',
            'estado' => 'required',
        ]);
        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }
        $sopas = Sopa::find($id);
        if (is_null($sopas)) {
            return $this->sendError('Sopa no encontrado.');
        }
        $sopas->nombre = $input['nombre'];
        $sopas->estado = $input['estado'];
        $sopas->save();
        return $this->sendResponse($sopas->toArray(), 'Sopa actualizado correctamente.');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $sopas = Sopa::find($id);
        if (is_null($sopas)) {
            return $this->sendError('Sopa no encontrado.');
        }
        $sopas->delete();
        return $this->sendResponse($id, 'Eliminado con exito.');
    }
}