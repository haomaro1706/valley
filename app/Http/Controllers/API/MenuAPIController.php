<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\API\APIBaseController as APIBaseController;
use App\Menu;
use Validator;

class MenuAPIController extends APIBaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $menus = Menu::all();
        return $this->sendResponse($menus->toArray(), 'Menus listados con exito.');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $validator = Validator::make($input, [
            'nombre' => 'required',
            'estado' => 'required'
        ]);
        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }
        $menus = Menu::create($input);
        return $this->sendResponse($menus->toArray(), 'Menu creado correctamente.');
    }
    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $menus = Menu::find($id);
        if (is_null($menus)) {
            return $this->sendError('Menu no encontrado.');
        }
        return $this->sendResponse($menus->toArray(), 'Menus generados correctamente.');
    }
    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->all();
        $validator = Validator::make($input, [
            'nombre' => 'required',
            'estado' => 'required',
        ]);
        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }
        $menus = Menu::find($id);
        if (is_null($menus)) {
            return $this->sendError('Menu no encontrado.');
        }
        $menus->nombre = $input['nombre'];
        $menus->estado = $input['estado'];
        $menus->save();
        return $this->sendResponse($menus->toArray(), 'Menu actualizado correctamente.');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $menus = Menu::find($id);
        if (is_null($menus)) {
            return $this->sendError('Menu no encontrado.');
        }
        $menus->delete();
        return $this->sendResponse($id, 'Eliminado con exito.');
    }
}