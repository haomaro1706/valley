<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\API\APIBaseController as APIBaseController;
use App\Proteina;
use Validator;

class ProteinaAPIController extends APIBaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $Proteinas = Proteina::all();
        return $this->sendResponse($Proteinas->toArray(), 'Proteinas listados con exito.');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $validator = Validator::make($input, [
            'nombre' => 'required',
            'estado' => 'required'
        ]);
        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }
        $Proteinas = Proteina::create($input);
        return $this->sendResponse($Proteinas->toArray(), 'Proteina creado correctamente.');
    }
    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $Proteinas = Proteina::find($id);
        if (is_null($Proteinas)) {
            return $this->sendError('Proteina no encontrado.');
        }
        return $this->sendResponse($Proteinas->toArray(), 'Proteinas generados correctamente.');
    }
    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->all();
        $validator = Validator::make($input, [
            'nombre' => 'required',
            'estado' => 'required',
        ]);
        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }
        $Proteinas = Proteina::find($id);
        if (is_null($Proteinas)) {
            return $this->sendError('Proteina no encontrado.');
        }
        $Proteinas->nombre = $input['nombre'];
        $Proteinas->estado = $input['estado'];
        $Proteinas->save();
        return $this->sendResponse($Proteinas->toArray(), 'Proteina actualizado correctamente.');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $Proteinas = Proteina::find($id);
        if (is_null($Proteinas)) {
            return $this->sendError('Proteina no encontrado.');
        }
        $Proteinas->delete();
        return $this->sendResponse($id, 'Eliminado con exito.');
    }
}