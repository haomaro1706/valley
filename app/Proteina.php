<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Proteina extends Model{

    protected $fillable = ['nombre', 'estado'];

}